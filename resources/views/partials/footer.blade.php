<footer class="pt-5">
	<div class="container">
		<div class="row">
			<div class="col-sm">
				<h5 class="h5 text-white">Navigatie</h5>
				<ul>
					<li><a href="/">Acasa</a></li>
					<li><a href="/login">Autentificare</a></li>
					<li><a href="#">Inregistrare</a></li>
				</ul>
			</div>
			<div class="col-sm">
				<h5 class="h5 text-white">Categorii</h5>
				<ul>
					<li><a href="#">Graphics &amp; Design</a></li>
					<li><a href="#">Programming &amp; Tech</a></li>
					<li><a href="#">Digital Marketing</a></li>
					<li><a href="#">Writing &amp; Translation</a></li>
					<li><a href="#">Video &amp; Animation</a></li>
					<li><a href="#">Admin Support</a></li>
					<li><a href="#">Architecture &amp; Engineering</a></li>
					<li><a href="#">Management &amp; Finance</a></li>
				</ul>
			</div>

		</div>
		<div class="row mt-5">
			<div class="col footer">
				<div class="col-sm copyright">
					<h6 class="h6">© <script type="text/javascript" >var year = new Date();document.write(year.getFullYear());</script> Angrro.ro. Toate Drepturile Rezervate</h6>
				</div>
				<div class="col-sm socialicons">
					<i class="px-1 fab fa-facebook fa-3x"></i>
					<i class="px-1 fab fa-instagram fa-3x"></i>
					<i class="px-1 fab fa-linkedin fa-3x"></i>
				</div>
			</div>
		</div>
	</div>
</footer>